import csv, math, os, os.path
from datetime import datetime
from argparse import ArgumentParser
from multiprocessing import Process, Array
from comparators.FastTextComparator import FastTextcomparator
from utils import parallel

QUESTION1_COL = 4
QUESTION2_COL = 5


def create_comparator(technique, questions):
    if technique == 'cos':
        pass
    elif technique == 'tf-idf':
        pass
    elif technique == 'w2v':
        pass
    elif technique == 'ft':
        comparator = FastTextcomparator()
    elif technique == 'sem':
        pass
    elif technique == 'ens':
        pass

    if comparator.must_train():
        questions_run_dir = os.path.join('internal', 'questions')
        questions_run_file = os.path.join(questions_run_dir, 'questions.txt')

        if not os.path.exists(questions_run_dir):
            os.makedirs(questions_run_dir)

        create_questions_file(questions_run_file, questions)

        comparator.train(questions_run_file)

    return comparator


def create_questions_file(file_name, questions):
    # Creates a file with the questions
    with open(file_name, 'w') as questions_file:
        # for pair_id in pair_ids:
        for row in questions:
            # Writes the 2 questions of each pair in a file.
            # This file will be used to train the comparator.
            questions_file.write(row[1] + '\n')
            questions_file.write(row[1] + '\n')


def distribute_comparing_work(question, questions, distances, num_workers, comparator):
    total = len(questions)
    matrix_size = math.ceil(total / num_workers)
    index_from = 0
    index_to = matrix_size

    workers = []
    for i in range(num_workers):
        # worker = Process(target=parallel.compare, args=(questions[i], distances, comparator, 1, 2))
        # worker.start()
        # workers.append(worker)
        worker = Process(target=parallel.compare_question,
                         args=(question, questions[index_from:index_to], distances, comparator))
        worker.start()
        workers.append(worker)

        index_from += matrix_size
        index_to += matrix_size if index_to + matrix_size <= total else total

    # Waits until the workers finish their work
    for worker in workers:
        worker.join()

if __name__ == '__main__':
    start_time = datetime.now()
    print('[' + datetime.now().strftime('%H:%M:%S') + '] Starting script.')

    parser = ArgumentParser('Computes the distance between all the questions')

    parser.add_argument('-t', dest='technique',      required=True, choices=['cos', 'tfidf', 'w2v', 'ft', 'sem', 'ens'])
    parser.add_argument('-q', dest='quora_path',     required=True)
    parser.add_argument('-w', dest='number_workers', default=5, type=int)

    args = parser.parse_args()
    technique = args.technique
    quora_path = args.quora_path
    num_workers = args.number_workers

    print('[' + datetime.now().strftime('%H:%M:%S') + '] Loading file...')

    questions = []
    with open(quora_path, 'r') as tsvin:
        reader = csv.reader(tsvin, delimiter='\t')

        next(reader)
        i = 0
        for row in reader:
            questions.append([i] + row[QUESTION1_COL:QUESTION1_COL+1])
            i += 1
            questions.append([i] + row[QUESTION2_COL:QUESTION2_COL+1])
            i += 1

    comparator = create_comparator(technique, questions)

    if not os.path.exists('results'):
            os.mkdir('results')

    results_path = os.path.join('results', 'distanceMatrix_'
                   + technique + '_' + '{:%Y%m%d_%H:%M:%S}'.format(datetime.now()) + '.csv')

    total = len(questions)
    with open(results_path, 'w') as results_file:
        writer = csv.writer(results_file, quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for i, row in enumerate(questions):
            print('[' + datetime.now().strftime('%H:%M:%S') + '] Comparing ' + str(i+1) + ' of ' + str(total))

            distances = Array('f', len(questions))

            distribute_comparing_work(row[1], questions, distances, num_workers, comparator)

            writer.writerow(["%.4f" % distance for distance in distances])

            if i == 3:
                break

    print('[' + datetime.now().strftime('%H:%M:%S') + '] Script finished. Total time: ' +
          str(datetime.now() - start_time))




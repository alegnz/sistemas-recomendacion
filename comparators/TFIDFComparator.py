from comparators.Comparator import Comparator
from utils.StopWords import StopWords
from sklearn.feature_extraction.text import TfidfVectorizer
import math
import numpy as np


class TFIDFComparator():
    def __init__(self):
        '''Intialize and train model with
        param preguntas: pair of questions of the train data
        '''
        st = StopWords()
        stopwords = st.get_stopwords('stopwords.txt')
        # initialize model
        self.model = TfidfVectorizer(min_df=1, ngram_range=(1, 3), stop_words=stopwords)

    def train(self, questions_path):
        # open questions_path file and get a list from all the questions in it
        preguntas = []
        with open(questions_path, 'r') as ifile:
            preguntas = ifile.readlines()

        # train model with pair of questions
        self.model.fit_transform(preguntas)

    def must_train(self):
        return True

    def compare(self, pregunta1, pregunta2):
        ''' Calculate distance between pregunta1 and pregunta2
        '''
        # set list of size 2 to send to model
        preguntas = []
        preguntas.append(pregunta1)
        preguntas.append(pregunta2)

        # get tfidf representation of the pair of questions given
        rep_preguntas = self.model.transform(preguntas)
        rep_preguntas = rep_preguntas.toarray()

        # get euclidean distance
        distance = np.linalg.norm(rep_preguntas[0] - rep_preguntas[1])

        # normalize distance with: min value = 0, max value = sqrt(2)
        distance = distance / math.sqrt(2)

        return distance

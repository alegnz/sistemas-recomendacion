import argparse
from utiles import StringUtils as su
from utiles.ManejadorCSV import ManejadorCSV
import csv

""" Toma el archivo de quora, limpia las preguntas
    y prepara un nuevo archivo de columnas:
        -ID par
        -Pregunta 1
        -Pregunta 2
        -Relacion (0 o 1)
"""

parser = argparse.ArgumentParser(description='Limpia las preguntas')
parser.add_argument('--quora', '-q', dest='archivo_quora', action='store', required=True, help='Archivo quora')
parser.add_argument('--salida', '-s', dest='archivo_salida', action='store', required=True, help='Archivo de salida')

args = parser.parse_args()

# manejador = ManejadorCSV()
# dataset = manejador.leer_archivo(args.archivo_quora)
# resultados = []
#
# for fila in dataset:
#     resultado = []
#     resultado.append(fila[0])                   # ID par
#     resultado.append(su.limpiar_texto(fila[3])) # Pregunta 1
#     resultado.append(su.limpiar_texto(fila[4])) # Pregunta 2
#     resultado.append(fila[5])                   # Relacion
#
#     resultados.append(resultado)
#
# manejador.escribir_archivo('pares_preguntas.csv', resultados)


with open(args.archivo_quora, 'rb') as tsvin, open(args.archivo_salida, 'wb') as csvout:
    reader = csv.reader(tsvin, delimiter='\t')
    writer = csv.writer(csvout)

    for fila in reader:
        print('Par ' + str(fila[0]))

        resultado = []
        resultado.append(fila[0])                   # ID par
        resultado.append(su.limpiar_texto(fila[3])) # Pregunta 1
        resultado.append(su.limpiar_texto(fila[4])) # Pregunta 2
        resultado.append(fila[5])                   # Relacion

        writer.writerow(resultado)
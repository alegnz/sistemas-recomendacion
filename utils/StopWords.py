import re

class StopWords():
    DIR_ARCHIVOS = 'data/'

    def __init__(self):
        pass

    def get_stopwords(self, nombre_archivo, path=DIR_ARCHIVOS):
        """Metodo que a partir de un archivo txt en el que cada linea es un stopword,
        devuelve una lista de dichos stopwords"""
        list = []
        with open(self.DIR_ARCHIVOS + nombre_archivo, 'r') as inputfile:
            for line in inputfile:
                line = ''.join(line.split())
                list.append(line)
        return list
